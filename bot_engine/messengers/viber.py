import json
import logging
from datetime import datetime
from typing import Any, Dict, List, Optional, Union


from django.contrib.sites.models import Site
from django.db.models import Model
from django.http.request import HttpRequest
from viberbot import Api
from viberbot.api.bot_configuration import BotConfiguration
from viberbot.api import messages as vbm
from viberbot.api.messages.message import Message
from viberbot.api.messages.typed_message import TypedMessage
from viberbot.api import viber_requests as vbr
from viberbot.api.viber_requests.viber_request import ViberRequest

from .base_messenger import BaseMessenger
from ..errors import MessengerException, NotSubscribed, RequestsLimitExceeded
from .. import models


log = logging.getLogger(__name__)


class Viber(BaseMessenger):
    """
    IM connector for Viber Bot API
    """
    # region Interface

    def __init__(self, token: str, **kwargs):
        super().__init__(token, **kwargs)

        self.bot = Api(BotConfiguration(
            auth_token=token,
            name=kwargs.get('name'),
            avatar=kwargs.get('avatar'),
        ))

    def enable_webhook(self, url: str, **kwargs):
        return self.bot.set_webhook(url=url)

    def disable_webhook(self):
        return self.bot.unset_webhook()

    def get_account_info(self) -> Dict[str, Any]:
        # data = {
        #    "status":0,
        #    "status_message":"ok",
        #    "id":"pa:75346594275468546724",
        #    "name":"account name",
        #    "uri":"accountUri",
        #    "icon":"http://example.com",
        #    "background":"http://example.com",
        #    "category":"category",
        #    "subcategory":"sub category",
        #    "location":{
        #       "lon":0.1,
        #       "lat":0.2
        #    },
        #    "country":"UK",
        #    "webhook":"https://my.site.com",
        #    "event_types":[
        #       "delivered",
        #       "seen"
        #    ],
        #    "subscribers_count":35,
        #    "members":[
        #       {
        #          "id":"01234567890A=",
        #          "name":"my name",
        #          "avatar":"http://example.com",
        #          "role":"admin"
        #       }
        #    ]
        # }
        try:
            data = self.bot.get_account_info()
        except Exception as err:
            raise MessengerException(err)

        return {
            'id': data.get('id'),
            'username': data.get('name'),
            'uri': data.get('uri'),  # check this
            'info': data
        }

    def get_user_info(self, user_id: str, **kwargs) -> Dict[str, Any]:
        # data = {
        #   "id":"01234567890A=",
        #   "name":"John McClane",
        #   "avatar":"http://avatar.example.com",
        #   "country":"UK",
        #   "language":"en",
        #   "primary_device_os":"android 7.1",
        #   "api_version":1,
        #   "viber_version":"6.5.0",
        #   "mcc":1,
        #   "mnc":1,
        #   "device_type":"iPhone9,4"
        # }
        try:
            data = self.bot.get_user_details(user_id)
        except Exception as err:
            if 'failed with status: 12' in str(err):
                raise RequestsLimitExceeded(err)
            raise MessengerException(err)

        return {
            'id': data.get('id'),
            'username': data.get('name'),
            'avatar': data.get('avatar'),
            'info': data,
        }

    def parse_message(self, request: HttpRequest) -> 'models.Message':
        # Verify signature
        sign = request.META.get('HTTP_X_VIBER_CONTENT_SIGNATURE')
        data = json.loads(request.body)
        log.debug(f'parse_message; {data}')
        if not self.bot.verify_signature(request.body, sign):
            raise MessengerException(f'Viber message not verified; '
                                     f'Data={request.body}; Sign={sign};')

        # return self._from_viber_message(self.bot.create_request(data))
        return self._from_viber_message(self.bot.parse_request(request.body), data.get('message_token'))

    def send_message(self, receiver: str,
                     messages: Union[models.Message, List[models.Message]]) -> List[str]:
        if isinstance(messages, models.Message):
            messages = [messages]

        vb_messages = []
        for message in messages:
            vb_messages.append(self._to_viber_message(message))

        try:
            return self.bot.send_messages(receiver, vb_messages)
        except Exception as err:
            if 'failed with status: 6, message: notSubscribed' in str(err):
                raise NotSubscribed(err)
            raise MessengerException(err)

    def welcome_message(self, text: str) -> Union[str, Dict[str, Any], None]:
        return {
            "sender": {
                "name": self.name,
                "avatar": self.avatar_url
            },
            "type": "text",
            "text": text
        }

    # endregion

    # region Help methods

    @staticmethod
    def _from_viber_message(vb_request: ViberRequest, message_token: str = None) -> 'models.Message':
        timestamp = datetime.utcfromtimestamp(vb_request.timestamp / 1000)

        if isinstance(vb_request, vbr.ViberMessageRequest):
            assert isinstance(vb_request.message, TypedMessage)

            vb_message = vb_request.message
            if isinstance(vb_message, vbm.TextMessage):
                if 'btn-' in vb_message.text:
                    return models.InButton(
                        id=vb_request.message_token, user_id=vb_request.sender.id,
                        timestamp=timestamp, command=vb_message.text
                    )
                return models.Text(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp, text=vb_message.text
                )
            elif isinstance(vb_message, vbm.PictureMessage):
                return models.Picture(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp, file_url=vb_message.media
                )
            elif isinstance(vb_message, vbm.VideoMessage):
                return models.Video(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp, file_url=vb_message.media,
                    file_size=vb_message.size
                )
            elif isinstance(vb_message, vbm.FileMessage):
                return models.File(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp, file_url=vb_message.media,
                    file_size=vb_message.size
                )
            elif isinstance(vb_message, vbm.RichMediaMessage):
                return models.RichMedia(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp, text=vb_message.alt_text,
                    rich_media=vb_message.rich_media
                )
            elif isinstance(vb_message, vbm.ContactMessage):
                return models.Contact(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp, contact=vb_message.contact
                )
            elif isinstance(vb_message, vbm.LocationMessage):
                return models.Location(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp, location=vb_message.location
                )
            elif isinstance(vb_message, vbm.URLMessage):
                return models.Url(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp, url=vb_message.media
                )
            elif isinstance(vb_message, vbm.StickerMessage):
                return models.Sticker(
                    id=vb_request.message_token, user_id=vb_request.sender.id,
                    timestamp=timestamp,
                    file_id=vb_message.sticker_id
                )
            return models.Text(
                id=vb_request.message_token, user_id=vb_request.sender.id,
                timestamp=timestamp, text=str(vb_message)
            )
        elif isinstance(vb_request, vbr.ViberConversationStartedRequest):
            return models.Start(
                id=vb_request.message_token, user_id=vb_request.user.id,
                timestamp=timestamp, user_name=vb_request.user.name,
                context=vb_request.context
            )
        elif isinstance(vb_request, vbr.ViberSubscribedRequest):
            return models.Subscribed(
                id=message_token, user_id=vb_request.user.id,
                timestamp=timestamp, user_name=vb_request.user.name
            )
        elif isinstance(vb_request, vbr.ViberUnsubscribedRequest):
            return models.Unsubscribed(
                id=message_token, user_id=vb_request.user_id,
                timestamp=timestamp
            )
        elif isinstance(vb_request, vbr.ViberDeliveredRequest):
            return models.Delivered(
                id=vb_request.message_token, user_id=vb_request.user_id,
                timestamp=timestamp
            )
        elif isinstance(vb_request, vbr.ViberSeenRequest):
            return models.Seen(
                id=vb_request.message_token, user_id=vb_request.user_id,
                timestamp=timestamp
            )
        elif isinstance(vb_request, vbr.ViberFailedRequest):
            log.warning(f'Client failed receiving message; Error={vb_request}')
            return models.Start(
                id=vb_request.message_token, user_id=vb_request.user_id,
                timestamp=timestamp, context=vb_request.desc
            )
        elif vb_request.event_type == 'webhook':
            return models.Webhook(timestamp=timestamp)

        log.warning(f'ViberRequest type={type(vb_request)}; '
                    f'Object={vb_request};')
        return models.Text(timestamp=timestamp, text=str(vb_request))

    def _to_viber_message(self, message: 'models.Message') -> Message:
        kb = self._get_keyboard(message.buttons)

        if isinstance(message, models.Text):
            return vbm.TextMessage(text=message.text, keyboard=kb)
        if isinstance(message, models.Sticker):
            return vbm.StickerMessage(sticker_id=message.file_id, keyboard=kb)
        elif isinstance(message, models.Picture):
            return vbm.PictureMessage(
                media=message.file_url, text=message.text, keyboard=kb
            )
        elif isinstance(message, models.Video):
            return vbm.VideoMessage(
                media=message.file_url, size=message.file_size,
                text=message.text, keyboard=kb
            )
        elif isinstance(message, (models.File, models.Audio)):
            return vbm.FileMessage(
                media=message.file_url, size=message.file_size or 0,
                file_name=message.file_name or '', keyboard=kb
            )
        elif isinstance(message, models.Contact):
            contact = message.contact
            return vbm.ContactMessage(contact=contact, keyboard=kb)
        elif isinstance(message, models.Url):
            return vbm.URLMessage(media=message.url, keyboard=kb)
        elif isinstance(message, models.Location):
            location = message.location
            return vbm.LocationMessage(location=location, keyboard=kb)
        elif isinstance(message, models.RichMedia):
            rich_media = message.rich_media
            return vbm.RichMediaMessage(
                rich_media=rich_media, alt_text=message.text, keyboard=kb
            )

    @staticmethod
    def _get_keyboard(buttons: List['models.OutButton']) -> Optional[Dict[str, Any]]:
        # TODO do refactoring
        if not buttons:
            return None

        vb_buttons = []
        for button in buttons:
            # assert isinstance(button, Button), f'{button=} {type(button)}'
            vb_btn = {
                'Columns': 2,  # TODO: how is it storage in Model?
                'Rows': 1,
                'BgColor': '#aaaaaa',
                'ActionType': 'reply',
                'ActionBody': button.command,
                'Text': '<font color="{clr}"><b>{text}'
                        '</b></font>'.format(text=button.text, clr='#131313'),
                'TextVAlign': 'middle', 'TextHAlign': 'center',
                'TextOpacity': 60, 'TextSize': 'large',
                'TextPaddings': [12, 8, 8, 20],  # [up, left, right, bottom]
            }

            if hasattr(button, 'image'):
                domain = Site.objects.get_current().domain
                vb_btn.update({
                    'BgMedia': f'https://{domain}{button.image}',
                    'BgMediaScaleType': 'fill'
                })

            vb_buttons.append(vb_btn)

        return {
            'Type': 'keyboard',
            'BgColor': '#ffffff',
            'min_api_version': 6,
            'Buttons': vb_buttons,
        }

    # endregion
