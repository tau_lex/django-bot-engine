from __future__ import annotations

import json
from datetime import datetime
import logging
from typing import Any, Callable, List, Optional, Type, Union
from uuid import uuid4, UUID

from django.conf import settings
from django.contrib.sites.models import Site
from django.db import models
from django.db.models import Q
from django.http.request import HttpRequest
from django.urls import reverse
from django.utils.module_loading import import_string
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from sortedm2m.fields import SortedManyToManyField

from .errors import MessengerException, NotSubscribed, RequestsLimitExceeded
from .messengers import BaseMessenger


# __all__ = ('Account', 'OutButton', 'Menu', 'Messenger', 'Message')

log = logging.getLogger(__name__)

ECHO_HANDLER = 'bot_engine.bot_handlers.simple_echo'
BUTTON_HANDLER = 'bot_engine.bot_handlers.button_echo'
BASE_HANDLER = ECHO_HANDLER


# region Message

class MessageState(models.TextChoices):
    NONE = 'NONE', 'Undefined'
    ERROR = 'ERROR', 'Error'
    EN_ROUTE = 'EN_ROUTE', 'Sending'
    NOT_DELIVERED = 'NOT_DELIVERED', 'NotDelivered'
    DELIVERED = 'DELIVERED', 'Delivered'
    SEEN = 'SEEN', 'Seen'


class MessageKind(models.TextChoices):
    NONE = 'NONE', 'None'
    INCOMING = 'INCOMING', 'Incoming'
    OUTGOING = 'OUTGOING', 'Outgoing'


class MessageType(models.TextChoices):
    NONE = 'NONE', 'None'

    # Messages
    TEXT = 'TEXT', 'Text'
    CONTACT = 'CONTACT', 'Contact'
    LOCATION = 'LOCATION', 'Location'
    RICHMEDIA = 'RICHMEDIA', 'RichMedia'
    URL = 'URL', 'Url'
    BUTTON = 'BUTTON', 'Button'
    FILE = 'FILE', 'File'
    PICTURE = 'PICTURE', 'Picture'
    STICKER = 'STICKER', 'Sticker'
    AUDIO = 'AUDIO', 'Audio'
    VIDEO = 'VIDEO', 'Video'

    # Events
    START = 'START', 'start'
    SUBSCRIBED = 'SUBSCRIBED', 'subscribed'
    UNSUBSCRIBED = 'UNSUBSCRIBED', 'unsubscribed'
    DELIVERED = 'DELIVERED', 'delivered'
    SEEN = 'SEEN', 'seen'
    WEBHOOK = 'WEBHOOK', 'webhook'
    FAILED = 'FAILED', 'failed'
    UNDEFINED = 'UNDEFINED', 'undefined'


class Message(models.Model):
    msg_id = models.CharField(
        'ID сообщения', max_length=64,
        default='')
    reply_to_msg = models.CharField(
        'Ответ на..', max_length=64,
        default='', null=True, blank=True)
    timestamp = models.DateTimeField(
        'Дата сообщения')
    text = models.TextField(
        'Текст сообщения', max_length=2048,
        default='', null=True, blank=True)
    contact = models.JSONField(
        'Контакт', default=dict,
        null=True, blank=True)
    location = models.JSONField(
        'Место расположения', default=dict,
        null=True, blank=True)
    rich_media = models.JSONField(
        'Мультимедиа', default=dict,
        null=True, blank=True)
    url = models.CharField(
        'Адрес сообщения', max_length=256,
        default='', null=True, blank=True)
    command = models.CharField(
        'Команда', max_length=64,
        default='', null=True, blank=True)
    file_id = models.CharField(
        'ИД файла', max_length=256,
        default='', null=True, blank=True)
    file_url = models.CharField(
        'Адрес файла', max_length=256,
        default='', null=True, blank=True)
    file_size = models.PositiveIntegerField(
        'Размер файла', default=0,
        null=True, blank=True)
    file_name = models.CharField(
        'Название файла', max_length=256,
        default='', null=True, blank=True)
    file_mime_type = models.CharField(
        'Тип контента', max_length=64,
        default='', null=True, blank=True)
    file_duration = models.PositiveIntegerField(
        'Продолжительность файла', default=0,
        null=True, blank=True)
    is_voice = models.BooleanField(
        'Голосовое', default=False)
    is_video_note = models.BooleanField(
        'Видео примечание', default=False)
    context = models.TextField(
        'Контекст', max_length=256,
        default='', null=True, blank=True)

    # from Event
    # user_name: str = None

    status = models.CharField(
        'Статус', max_length=32,
        default=MessageState.NONE,
        choices=MessageState.choices)
    kind = models.CharField(
        'Вид сообщения', max_length=32,
        default=MessageKind.NONE,
        choices=MessageKind.choices)
    type = models.CharField(
        'Тип сообщения', max_length=32,
        default=MessageType.NONE,
        choices=MessageType.choices)

    account = models.ForeignKey(
        'Account', models.CASCADE, null=True, blank=True,
        related_name='messages', verbose_name='Аккаунт')
    messenger = models.ForeignKey(
        'Messenger', models.CASCADE, null=True, blank=True,
        related_name='messages', verbose_name='Мессенджер')

    buttons = None
    user_id = None
    user_name = None

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ('-timestamp',)
        unique_together = ('msg_id', 'id')

    def add_button(self, button_list: Union[OutButton, List[OutButton]]):
        if not isinstance(self.buttons, list):
            self.buttons = []
        if not isinstance(button_list, list):
            button_list = [button_list]
        self.buttons = self.buttons + button_list

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.save()


# region Proxy Models

class Text(Message):
    class Meta:
        proxy = True

    def __init__(self, text: str,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 reply_to_msg: str = None, buttons: list = None,
                 *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.TEXT,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class Contact(Message):
    class Meta:
        proxy = True

    def __init__(self, contact: dict,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 text: str = None, reply_to_msg: str = None, buttons: list = None,
                 *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.CONTACT,
            contact=contact,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class Location(Message):
    class Meta:
        proxy = True

    def __init__(self, location: dict,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 text: str = None, reply_to_msg: str = None, buttons: list = None,
                 *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.LOCATION,
            location=location,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class RichMedia(Message):
    class Meta:
        proxy = True

    def __init__(self, rich_media: dict,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 text: str = None, reply_to_msg: str = None, buttons: list = None,
                 *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.RICHMEDIA,
            rich_media=rich_media,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class Url(Message):
    class Meta:
        proxy = True

    def __init__(self, url: str,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 text: str = None, reply_to_msg: str = None, buttons: list = None,
                 *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.URL,
            url=url,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class InButton(Message):
    class Meta:
        proxy = True

    def __init__(self, command: str, text: str = None,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 reply_to_msg: str = None, *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.BUTTON,
            command=command,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        super().__init__(*args, **kwargs)


class File(Message):
    class Meta:
        proxy = True

    def __init__(self,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 file_id: str = None, file_url: str = None, file_size: int = None,
                 file_name: str = None, file_mime_type: str = None, text: str = None,
                 reply_to_msg: str = None, buttons: list = None, *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.FILE,
            file_id=file_id,
            file_url=file_url,
            file_size=file_size,
            file_name=file_name,
            file_mime_type=file_mime_type,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class Picture(Message):
    class Meta:
        proxy = True

    def __init__(self,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 file_id: str = None, file_url: str = None, file_size: int = None,
                 file_name: str = None, file_mime_type: str = None, text: str = None,
                 reply_to_msg: str = None, buttons: list = None, *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.PICTURE,
            file_id=file_id,
            file_url=file_url,
            file_size=file_size,
            file_name=file_name,
            file_mime_type=file_mime_type,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class Sticker(Message):
    class Meta:
        proxy = True

    def __init__(self,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 file_id: str = None,
                 file_url: str = None, file_size: int = None, file_name: str = None,
                 file_mime_type: str = None, text: str = None, reply_to_msg: str = None,
                 buttons: list = None, *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.STICKER,
            file_id=file_id,
            file_url=file_url,
            file_size=file_size,
            file_name=file_name,
            file_mime_type=file_mime_type,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class Audio(Message):
    class Meta:
        proxy = True

    def __init__(self,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 file_id: str = None,
                 file_url: str = None, file_size: int = None, file_name: str = None,
                 file_mime_type: str = None, file_duration: int = None,
                 is_voice: bool = False, text: str = None, reply_to_msg: str = None,
                 buttons: list = None, *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.AUDIO,
            file_id=file_id,
            file_url=file_url,
            file_size=file_size,
            file_name=file_name,
            file_mime_type=file_mime_type,
            file_duration=file_duration,
            is_voice=is_voice,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


class Video(Message):
    class Meta:
        proxy = True

    def __init__(self,
                 id: str = None, user_id: str = None, timestamp: datetime = None,
                 file_id: str = None,
                 file_url: str = None, file_size: int = None, file_name: str = None,
                 file_mime_type: str = None, file_duration: int = None,
                 is_video_note: bool = False, text: str = None, reply_to_msg: str = None,
                 buttons: list = None, *args, **kwargs):
        if timestamp is None:
            timestamp = datetime.utcnow()
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.VIDEO,
            file_id=file_id,
            file_url=file_url,
            file_size=file_size,
            file_name=file_name,
            file_mime_type=file_mime_type,
            file_duration=file_duration,
            is_video_note=is_video_note,
            text=text,
            reply_to_msg=reply_to_msg,
        )
        self.user_id = user_id
        self.buttons = buttons
        super().__init__(*args, **kwargs)


# Events

class Start(Message):
    class Meta:
        proxy = True

    def __init__(self, timestamp: datetime, id: str = None, user_id: str = None,
                 user_name: str = None, *args, **kwargs):
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.START,
        )
        self.user_id = user_id
        self.user_name = user_name
        super().__init__(*args, **kwargs)


class Subscribed(Message):
    class Meta:
        proxy = True

    def __init__(self, timestamp: datetime, id: str = None, user_id: str = None,
                 user_name: str = None, *args, **kwargs):
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.SUBSCRIBED,
        )
        self.user_id = user_id
        self.user_name = user_name
        super().__init__(*args, **kwargs)


class Unsubscribed(Message):
    class Meta:
        proxy = True

    def __init__(self, timestamp: datetime, id: str = None, user_id: str = None,
                 user_name: str = None, *args, **kwargs):
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.UNSUBSCRIBED,
        )
        self.user_id = user_id
        self.user_name = user_name
        super().__init__(*args, **kwargs)


class Delivered(Message):
    class Meta:
        proxy = True

    def __init__(self, timestamp: datetime, id: str = None, user_id: str = None,
                 user_name: str = None, *args, **kwargs):
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.DELIVERED,
        )
        self.user_id = user_id
        self.user_name = user_name
        super().__init__(*args, **kwargs)


class Seen(Message):
    class Meta:
        proxy = True

    def __init__(self, timestamp: datetime, id: str = None, user_id: str = None,
                 user_name: str = None, *args, **kwargs):
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.SEEN,
        )
        self.user_id = user_id
        self.user_name = user_name
        super().__init__(*args, **kwargs)


class Webhook(Message):
    class Meta:
        proxy = True

    def __init__(self, timestamp: datetime, id: str = None, user_id: str = None,
                 user_name: str = None, *args, **kwargs):
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.WEBHOOK,
        )
        self.user_id = user_id
        self.user_name = user_name
        super().__init__(*args, **kwargs)


class Failed(Message):
    class Meta:
        proxy = True

    def __init__(self, timestamp: datetime, id: str = None, user_id: str = None,
                 user_name: str = None, *args, **kwargs):
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.FAILED,
        )
        self.user_id = user_id
        self.user_name = user_name
        super().__init__(*args, **kwargs)


class Undefined(Message):
    class Meta:
        proxy = True

    def __init__(self, timestamp: datetime, id: str = None, user_id: str = None,
                 user_name: str = None, *args, **kwargs):
        kwargs.update(
            msg_id=id,
            timestamp=timestamp,
            type=MessageType.UNDEFINED,
        )
        self.user_id = user_id
        self.user_name = user_name
        super().__init__(*args, **kwargs)

# endregion

# endregion


class MessengerType(models.TextChoices):
    NONE = 'NONE', 'None'
    # FBMESSENGER = 'FBMESSENGER', 'FB Messenger'
    # SKYPE = 'SKYPE', 'Skype'
    # SLACK = 'slack'
    TELEGRAM = 'TELEGRAM', 'Telegram'
    VIBER = 'VIBER', 'Viber'
    # WECHAT = 'WECHAT', 'WeChat'
    # WHATSAPP = 'WHATSAPP', 'WhatsApp'

    @classmethod
    def messenger_classes(cls) -> dict:
        return {m_type: f'bot_engine.messengers.{m_type}.{m_type.capitalize()}'
                for m_type in cls if m_type != cls.NONE}

    @property
    def messenger_class(self) -> Optional[Type[BaseMessenger]]:
        if self == MessengerType.NONE:
            return None
        return import_string(f'bot_engine.messengers.{self.lower()}.{self.capitalize()}')


class Messenger(models.Model):
    title = models.CharField(
        _('title'), max_length=256,
        help_text=_('This name will be used as the sender name.'),
    )
    api_type = models.CharField(
        _('API type'), max_length=64,
        choices=MessengerType.choices,
        default=MessengerType.NONE,
    )
    token = models.CharField(
        _('API token'), max_length=256,
        default='', blank=True,
        help_text=_('Token or secret key.'),
    )
    proxy = models.CharField(
        _('proxy'), max_length=256,
        default='', blank=True,
        help_text=_('Enter proxy uri with format '
                    '"schema://user:password@proxy_address:port"'),
    )
    logo = models.CharField(
        _('logo'), max_length=256,
        default='', blank=True,
        help_text=_('Relative URL. Required for some messenger APIs: Viber.'),
    )
    welcome_text = models.TextField(
        _('welcome text'),
        default='', blank=True,
        help_text=_('Welcome message. Will be sent in response to the opening'
                    ' of the dialog (not a subscribe event). May be used with'
                    ' some messaging programs: Viber.'),
    )

    handler = models.CharField(
        _('main handler'), max_length=256,
        default=BASE_HANDLER, blank=True,
        help_text=_('It processes all messages that do not fall into '
                    'the menu and button handlers. To implement a handler, '
                    f'implement a {BASE_HANDLER} interface.'),
    )
    menu = models.ForeignKey(
        'Menu', models.SET_NULL,
        related_name='messengers',
        verbose_name=_('main menu'),
        null=True, blank=True,
        help_text=_('The root menu or start state. For example, "Home".'),
    )

    uuid = models.UUIDField(
        'UUID', default=uuid4,
        editable=False,
    )
    is_active = models.BooleanField(
        _('active'),
        default=False, editable=False,
        help_text=_('This flag changes when the webhook on the messenger API '
                    'server is activated/deactivated.'),
    )
    updated = models.DateTimeField(
        _('updated'), auto_now=True,
    )
    created = models.DateTimeField(
        _('created'), auto_now_add=True,
    )

    class Meta:
        verbose_name = _('messenger')
        verbose_name_plural = _('messengers')
        constraints = [
            models.UniqueConstraint(fields=('token', 'uuid'),
                                    name='Unique messenger')
        ]

    def __str__(self) -> str:
        return f'Messenger {self.title} ({self.api_type})'

    def __repr__(self) -> str:
        return f'<bot_engine.Messenger object ({self.id}:{self.api_type})>'

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.save()

    def get_url_activating_webhook(self) -> str:
        return reverse('bot_engine:activate', kwargs={'id': self.id})

    def get_url_deactivating_webhook(self) -> str:
        return reverse('bot_engine:deactivate', kwargs={'id': self.id})

    def get_uuid(self) -> Union[UUID, str]:
        if not self.uuid:
            self.uuid = uuid4()
            self.save()
        return self.uuid

    def dispatch(self, request: HttpRequest) -> Optional[Any]:
        """
        Entry point for current messenger account

        :param request: Django request object
        :type request: HttpRequest
        :return: Answer data (optional)
        :rtype: Optional[Any]
        """
        message = self.api.parse_message(request)
        log.debug(f'dispatch; {request=};')

        if message.user_id:
            account, created = Account.objects.get_or_create(
                id=message.user_id,
                defaults={
                    'username': (getattr(message, 'user_name', None)
                                 or message.user_id),
                }
            )
            if created:
                account.messengers_set.add(
                    self,
                    through_defaults={
                        'is_active': True,
                        'current_menu': self.menu
                    }
                )
                acc_msgr = account.messengers_x.get(messenger=self)
            else:
                acc_msgr, _ = account.messengers_x.get_or_create(
                    messenger=self,
                    defaults={
                        'is_active': True,
                        'current_menu': self.menu
                    }
                )
            if not account.info:
                account.update_info(self)
        else:
            account = None
            acc_msgr = None

        # Find buttons or something else
        message = self.preprocess_message(message, account)

        log.debug(f'Messenger.Dispatch; {self}; {message}; {account}; {acc_msgr};')

        if isinstance(message, (Start, Subscribed, Unsubscribed, Delivered, Seen, Webhook, Failed, Undefined)):
            return self.process_service_message(message, account, acc_msgr)

        if True:  # implement "save message" flag in module settings
            message.save()

        if acc_msgr and acc_msgr.current_menu:
            acc_msgr.current_menu.process_message(message, account, self)
        else:
            self.process_message(message, account)

    def preprocess_message(self, message: Message, account: Account) -> Message:
        """
        Pre-process message data
        Some messengers can understand the message only in context,
        e.g. Telegram(from text to button)

        :param message: bot_engine.Message object
        :param account: bot_engine.Account object
        :return: Message object
        """
        if self.api_type in [MessengerType.VIBER]:
            return message

        menu = account.messengers_x.get(messenger=self).current_menu
        log.debug(f'Messenger.preprocess_message; {menu}; {message.text}; {message.msg_id}')
        if (self.api_type in [MessengerType.TELEGRAM.value]
                and isinstance(message, Text) and menu):
            for button in menu.buttons.all():
                if message.text == button.text:
                    message = InButton(text=message.text, command=message.text, id=message.msg_id)
                    break

        return message

    def process_message(self, message: Message, account: Account):
        """
        Process the message with a bound handler.
        :param message: bot_engine.Massage object
        :type message: Message
        :param account: bot_engine.Account object
        :type account: Account
        :return: None
        """
        if self.handler:
            self.call_handler(message, account, self)

    def process_service_message(self, message: Message, account: Account, acc_msgr):
        """
        Process the system message.
        :param acc_msgr:
        :param message: bot_engine.Massage object
        :param account: bot_engine.Account object
        :return: None
        """
        # TODO: Make refactoring and implement service handler
        if isinstance(message, Start) and account and self.welcome_text:
            self.send_message(account, Text(text=self.welcome_text))
            return self.api.welcome_message(self.welcome_text)
        elif isinstance(message, Subscribed):
            acc_msgr.update(is_active=True)
        elif isinstance(message, Unsubscribed) and acc_msgr:
            acc_msgr.update(is_active=False)
        elif isinstance(message, Seen):
            try:
                obj_m = Message.objects.get(msg_id=message.msg_id)
                obj_m.update(status=MessageState.SEEN)
            except Message.DoesNotExist:
                log.debug(f'process_service_message; message {message.msg_id} not found')
        elif isinstance(message, Delivered):
            try:
                obj_m = Message.objects.get(msg_id=message.msg_id)
                obj_m.update(status=MessageState.DELIVERED)
            except Message.DoesNotExist:
                log.debug(f'process_service_message; message {message.msg_id} not found')

    @property
    def call_handler(self) -> Callable:
        # importing handler or hot reloading
        if not hasattr(self, '_handler') or self.handler != self._old_handler:
            self._handler = import_string(self.handler)
            self._old_handler = self.handler
        return self._handler

    def enable_webhook(self):
        domain = Site.objects.get_current().domain
        url = reverse('bot_engine:webhook', kwargs={'uuid': self.get_uuid()})
        return self.api.enable_webhook(url=f'https://{domain}{url}')

    def disable_webhook(self):
        return self.api.disable_webhook()

    @property
    def api(self) -> BaseMessenger:
        if not hasattr(self, '_api'):
            domain = Site.objects.get_current().domain
            url = self.logo
            self._api = self._api_class(
                self.token, proxy=self.proxy, name=self.title,
                avatar=f'https://{domain}{url}'
            )
        return self._api

    @property
    def _api_class(self) -> Type[BaseMessenger]:
        """
        Returns the connector class of selected type.
        """
        return MessengerType(self.api_type).messenger_class

    def send_message(self, account: Account, message: Union[Message, List[Message]],
                     buttons: Union[OutButton, List[OutButton]] = None):
        if isinstance(message, Message):
            message = [message]

        if buttons:
            message[-1].add_button(buttons)

        acc_msgr = account.messengers_x.get(messenger=self)
        if acc_msgr.current_menu:
            message[-1].add_button(acc_msgr.current_menu.button_list())
            if account.user and account.user.is_staff:
                message[-1].add_button(acc_msgr.current_menu.button_list(True))
        try:
            message_ids = self.api.send_message(account.id, message)
            for idx, msg in enumerate(message):
                msg.messenger = self
                msg.account = account
                msg.msg_id = message_ids[idx]
                msg.kind = MessageKind.OUTGOING
                msg.status = MessageState.EN_ROUTE
                if True:  # implement "save message" flag in module settings
                    msg.save()
        except NotSubscribed:
            acc_msgr.update(is_active=False)
            for idx, msg in enumerate(message):
                msg.messenger = self
                msg.account = account
                msg.kind = MessageKind.OUTGOING
                msg.status = MessageState.NOT_DELIVERED
                if True:  # implement "save message" flag in module settings
                    msg.save()
            log.warning(f'Messenger.send_message; Account {account} is not subscribed.')
        except MessengerException as err:
            for idx, msg in enumerate(message):
                msg.messenger = self
                msg.account = account
                msg.kind = MessageKind.OUTGOING
                msg.status = MessageState.ERROR
                if True:  # implement "save message" flag in module settings
                    msg.save()
            log.exception(f'Messenger.send_message; Error={err}.')


class AccountManager(models.Manager):
    def get_queryset(self):
        return (super().get_queryset()
                .select_related('user')
                .prefetch_related('messengers_x', 'messengers_x__current_menu'))


class Account(models.Model):
    id = models.CharField(
        _('account id'), max_length=256,
        primary_key=True, editable=False,
    )
    username = models.CharField(
        _('user name'), max_length=256,
        null=True, blank=True,
    )
    utm_source = models.CharField(
        _('utm source'), max_length=256,
        null=True, blank=True,
    )
    info = models.JSONField(
        _('information'),
        default=dict, blank=True,
    )
    context = models.JSONField(
        _('context'),
        default=dict, blank=True,
    )
    phone = models.CharField(
        _('phone'), max_length=20,
        null=True, blank=True,
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.SET_NULL,
        related_name='accounts',
        verbose_name=_('user'),
        null=True, blank=True,
    )
    messengers = models.ManyToManyField(
        Messenger,
        related_name='accounts',
        verbose_name=_('messengers'),
        through='AccountMessenger',
        through_fields=('account', 'messenger'),
        blank=True,
    )

    updated = models.DateTimeField(
        _('last visit'), auto_now=True,
    )
    created = models.DateTimeField(
        _('first visit'), auto_now_add=True,
    )

    objects = AccountManager()

    class Meta:
        verbose_name = _('account')
        verbose_name_plural = _('accounts')

    def __str__(self) -> str:
        return f'Account @{self.username or self.id}'

    def __repr__(self) -> str:
        return f'<bot_engine.Account object ({self.id})>'

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.save()

    @property
    def avatar(self) -> str:
        return self.info.get('avatar') or ''

    def update_info(self, messenger: Messenger = None):
        try:
            _messenger = messenger or self.messengers.all()[0]
            user_info = _messenger.api.get_user_info(
                self.id, chat_id=self.id
            )
            self.update(
                username=user_info.get('username'),
                info=user_info.get('info')
            )
        except RequestsLimitExceeded as err:
            self.info['error'] = str(err)
            # self.update(username=message.sender.username,
            #             info=requestLimit)
            log.exception(err)


class Menu(models.Model):
    title = models.CharField(
        _('title'), max_length=256,
    )
    message = models.TextField(
        _('message'),
        null=True, blank=True,
        help_text=_('The text of the message sent when you get to this menu.'),
    )
    handler = models.CharField(
        _('handler'), max_length=256,
        default='', blank=True,
        help_text=_(f'Your handler implementation must implement '
                    f'the {ECHO_HANDLER} interface.'),
    )
    comment = models.TextField(
        _('comment'),
        null=True, blank=True,
        help_text=_('Comment text. Does not affect functionality.'),
    )

    buttons = SortedManyToManyField(
        'OutButton', blank=True,
        sort_value_field_name='order',
        verbose_name=_('buttons'), related_name='menus',
    )

    updated = models.DateTimeField(
        _('updated'), auto_now=True,
    )
    created = models.DateTimeField(
        _('created'), auto_now_add=True,
    )

    class Meta:
        verbose_name = _('menu')
        verbose_name_plural = _('menus')

    def __str__(self) -> str:
        return f'Menu {self.title}'

    def __repr__(self) -> str:
        return f'<bot_engine.Menu object ({self.id})>'

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.save()

    def process_message(self, message: Message, account: Account,
                        messenger: Messenger):
        """
        Process the message with a bound handler.

        :param message: bot_engine.Massage object
        :type message: Message
        :param account: bot_engine.Account object
        :type account: Account
        :param messenger: bot_engine.Messenger object
        :type messenger: Messenger
        :return: None
        """
        # TODO check process
        # TODO make permissions filter
        log.debug(f'Menu.process_message; {self}; message={message.text}, type={type(message)};')

        if isinstance(message, InButton):
            if len(self.buttons.all()) == 0:
                buttons = OutButton.objects.filter(
                    Q(command=message.command) | Q(text=message.text)
                ).all()
            else:
                buttons = self.buttons.filter(
                    Q(command=message.command) | Q(text=message.text)
                ).all()

            log.debug(f'Menu.process_message; Text={message.text} '
                      f'Command={message.command}; Buttons={buttons};')

            if buttons:
                return buttons[0].process_button(message, account, messenger)

            if not buttons or len(buttons) > 1:
                log.warning('The number of buttons found is different from one.'
                            ' This can lead to unplanned behavior.'
                            ' We recommend making the buttons unique.')
        elif self.handler:
            self.call_handler(message, account, messenger)

    @property
    def call_handler(self) -> Callable:
        if not hasattr(self, '_handler'):
            self._handler = import_string(self.handler)
        return self._handler

    def button_list(self, for_staff: bool = False) -> List[OutButton]:
        return list(self.buttons.filter(is_inline=False, for_staff=for_staff).all())

    def i_button_list(self) -> List[OutButton]:
        return list(self.buttons.filter(is_inline=True).all())


class AccountMessenger(models.Model):
    account = models.ForeignKey(
        Account, models.CASCADE,
        related_name='messengers_x',
    )
    messenger = models.ForeignKey(
        Messenger, models.CASCADE,
        related_name='accounts_x',
    )

    is_active = models.BooleanField(
        _('active'),
        default=False, editable=False,
        help_text=_('This flag changes when the user account on '
                    'the messenger API server is subscribed/unsubscribed.'),
    )
    current_menu = models.ForeignKey(
        Menu, models.CASCADE,
        related_name='current_menus',
        null=True, blank=True,
    )

    updated = models.DateTimeField(
        _('updated'), auto_now=True,
    )
    created = models.DateTimeField(
        _('created'), auto_now_add=True,
    )

    class Meta:
        verbose_name = _('account messenger')
        verbose_name_plural = _('account messengers')
        constraints = [
            models.UniqueConstraint(
                fields=('account', 'messenger'), name='Unique relation',
            ),
        ]

    def __str__(self) -> str:
        return f'AccountMessenger {self.account_id}:{self.messenger_id}'

    def __repr__(self) -> str:
        return (f'<bot_engine.AccountMessenger object '
                f'({self.account_id}:{self.messenger_id})>')

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.save()


class OutButton(models.Model):
    title = models.CharField(
        _('title'), max_length=256,
    )
    text = models.CharField(
        _('text'), max_length=256,
        help_text=_('Button text displayed.'),
    )
    message = models.TextField(
        _('message'),
        null=True, blank=True,
        help_text=_('The text of the message sent during the processing of '
                    'a button click.'),
    )
    handler = models.CharField(
        _('handler'), max_length=256,
        default='', blank=True,
        help_text=_(f'Your handler implementation must implement '
                    f'the {BUTTON_HANDLER} interface.'),
    )
    comment = models.TextField(
        _('comment'), max_length=1024,
        null=True, blank=True,
        help_text=_('Comment text. Does not affect functionality.'),
    )

    next_menu = models.ForeignKey(
        'Menu', models.SET_NULL,
        verbose_name=_('next menu'), related_name='from_buttons',
        null=True, blank=True,
    )
    for_staff = models.BooleanField(
        _('for staff users'),
        default=False, blank=True,
        help_text=_('Buttons with this flag are available only for user '
                    'accounts of site staff (django.contrib.auth).'),
    )
    for_admin = models.BooleanField(
        _('for admin users'),
        default=False, blank=True,
        help_text=_('Buttons with this flag are available only for user '
                    'accounts of site admins (django.contrib.auth).'),
    )

    command = models.CharField(
        _('command'), max_length=256,
        default=None, null=True, editable=False,
    )
    is_inline = models.BooleanField(
        _('inline'), default=False,
        help_text=_('Inline in message.'),
    )
    is_active = models.BooleanField(
        _('active'), default=True,
    )
    updated = models.DateTimeField(
        _('updated'), auto_now=True,
    )
    created = models.DateTimeField(
        _('created'), auto_now_add=True,
    )

    class Meta:
        verbose_name = _('button')
        verbose_name_plural = _('buttons')
        constraints = [
            models.UniqueConstraint(fields=('command',),
                                    name='Unique button')
        ]
        db_table = 'bot_engine_button'

    def __str__(self) -> str:
        return f'Button {self.title}'

    def __repr__(self) -> str:
        return f'<bot_engine.Button object ({self.command})>'

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.save()

    def process_button(self, message: InButton, account: Account,
                       messenger: Messenger):
        """
        Process the message with a bound handler.

        :param message: bot_engine.Massage object
        :type message: Message
        :param account: bot_engine.Account object
        :type account: Account
        :param messenger: bot_engine.Messenger object
        :type messenger: Messenger
        :return: None
        """
        # TODO check process
        if self.message:
            messenger.send_message(account, Text(text=self.message))

        if self.next_menu:
            acc_msgr = account.messengers_x.get(messenger=messenger)  # fixme or make refactoring
            acc_msgr.update(current_menu=self.next_menu)
            btn_list = list(self.next_menu.buttons.all()) or None
            if self.next_menu.message:
                msg_text = self.next_menu.message
                messenger.send_message(account, Text(text=msg_text))
            else:
                messenger.send_message(account, Message(buttons=btn_list))

        log.debug(f'Button.process_button; handler={self.handler};')
        if self.handler:
            log.debug('Button.process_button; Call handler..;')
            self.call_handler(message, account, messenger)

    @property
    def call_handler(self) -> Callable:
        try:
            if not hasattr(self, '_handlers'):
                self._handlers = {}
            if not self._handlers.get(self.handler):
                self._handlers[self.handler] = import_string(self.handler)
            return self._handlers[self.handler]
        except (ImportWarning, ImportError, ModuleNotFoundError, RecursionError) as err:
            log.exception(err)

    def save(self, *args, **kwargs):
        if not self.command:
            rnd = uuid4().hex[::3]
            self.command = f'btn-{slugify(self.title)}-{rnd}'
        super().save(*args, **kwargs)

    @property
    def action(self) -> str:
        return self.handler or str(self.next_menu)

    def to_dict(self) -> dict:
        return {
            'text': self.title,
            'command': self.command,
            'size': (2, 1)
        }

