from . import bot
from .models import Account, Messenger
from .types import Message


@bot.handler
def simple_echo(message: Message, account: Account, messenger: Messenger):
    """
    Simple echo chat bot.
    """
    messenger.send_message(account, message)
    # TODO: implement the ability to add buttons to menu buttons


@bot.handler
def silent_handler(message: Message, account: Account, messenger: Messenger):
    """
    A simple handler with no response and no action.
    """
    pass


@bot.button_handler
def button_echo(message: Message, account: Account, messenger: Messenger):
    """
    Simple echo on the button.
    """
    messenger.send_message(account, message)
